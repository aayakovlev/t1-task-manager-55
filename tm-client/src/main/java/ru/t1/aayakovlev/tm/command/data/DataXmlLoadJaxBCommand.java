package ru.t1.aayakovlev.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.t1.aayakovlev.tm.dto.request.DataXmlLoadJaxBRequest;
import ru.t1.aayakovlev.tm.exception.AbstractException;

@Component
public final class DataXmlLoadJaxBCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-xml-jaxb-load";

    @NotNull
    private static final String DESCRIPTION = "Load data from xml file.";

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @SneakyThrows
    public void execute() throws AbstractException {
        System.out.println("[DATA LOAD XML]");
        domainEndpoint.xmlLoadJaxB(new DataXmlLoadJaxBRequest(getToken()));
    }

}
