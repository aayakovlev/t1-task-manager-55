package ru.t1.aayakovlev.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.aayakovlev.tm.command.AbstractCommand;
import ru.t1.aayakovlev.tm.dto.model.ProjectDTO;
import ru.t1.aayakovlev.tm.endpoint.ProjectEndpoint;
import ru.t1.aayakovlev.tm.enumerated.Status;

import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;

@Component
public abstract class AbstractProjectCommand extends AbstractCommand {

    @NotNull
    @Autowired
    protected ProjectEndpoint projectEndpoint;

    @Override
    @Nullable
    public String getArgument() {
        return null;
    }

    protected void renderProjects(@NotNull final List<ProjectDTO> projects) {
        @NotNull final AtomicInteger index = new AtomicInteger(1);
        projects.stream()
                .filter(Objects::nonNull)
                .forEachOrdered((p) -> System.out.println(index.getAndIncrement() + ". " + p));
    }

    protected void showProject(@Nullable final ProjectDTO project) {
        if (project == null) return;
        System.out.println("Id: " + project.getId());
        System.out.println("Name: " + project.getName());
        System.out.println("Description: " + project.getDescription());
        System.out.println("Status: " + Status.toName(project.getStatus()));
    }

}
