package ru.t1.aayakovlev.tm.command.data;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.aayakovlev.tm.command.AbstractCommand;
import ru.t1.aayakovlev.tm.endpoint.DomainEndpoint;

@Component
@NoArgsConstructor
public abstract class AbstractDataCommand extends AbstractCommand {

    @NotNull
    @Autowired
    protected DomainEndpoint domainEndpoint;

    @Override
    @Nullable
    public String getArgument() {
        return null;
    }

}
