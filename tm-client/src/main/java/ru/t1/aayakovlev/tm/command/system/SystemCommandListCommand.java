package ru.t1.aayakovlev.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.t1.aayakovlev.tm.command.AbstractCommand;

import java.util.Collection;

@Component
public final class SystemCommandListCommand extends AbstractSystemCommand {

    @NotNull
    public static final String ARGUMENT = "-cmd";

    @NotNull
    public static final String DESCRIPTION = "Show commands description.";

    @NotNull
    public static final String NAME = "command";

    @Override
    @NotNull
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[COMMAND LIST]");
        @NotNull final Collection<AbstractCommand> commands = commandService.getTerminalCommands();
        commands.stream()
                .filter((c) -> !c.getName().isEmpty())
                .forEachOrdered((c) -> System.out.println(c.getName() + ": " + c.getDescription()));
    }

}
